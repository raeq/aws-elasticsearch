'''
Lists AMIs using the AWS describe-images API.
Stores each result as an elasticsearch document.
The ImageId is used as the docuemnt's _id attribute.
Can optionally update / create AMIs released at -x days ago.
'''

import logging
import logging.config
import pickle
from datetime import datetime, timezone
import boto3
import elasticsearch
from dateutil import parser

URL = 'http://192.168.1.82:9200/'
AMIINDEX = "ami-index"
CANONICAL = '099720109477'
CENTOS = '679593333241'
RHEL = '309956199498'
FEDORA = '125523088429'
AMZLX2 = '137112412989'
SUSE = '013907871322'
AMZN_EKS = '602401143452'
INFOBLOX = '057670693668'
MOSTRECENT = 1


def listAMIs() -> list:
    '''
    @return: List of AMIs returned by the describe-images API method.
    @see: https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html
    '''
    session = boto3.session.Session(profile_name='master')
    prefix = 'all'
    images = ""
    fname = f"{prefix}_pickled_images.pickle"

    try:
        with open(fname, 'rb') as pfile:
            images = pickle.load(pfile)
        logging.debug(f"Loaded results from {fname}.")

    except FileNotFoundError:
        logging.debug("No saved file on disk, querying API.")
        # Name=description,Values=
        filters = [
            {'Name': 'image-type', 'Values': ['machine']},
            {'Name': 'name', 'Values': ['*']},
            {'Name': 'architecture', 'Values': ['x86_64']},
            {'Name': 'root-device-type', 'Values': ['ebs']},
        ]

        ec2 = session.client('ec2', region_name="us-east-1")
        images = (ec2.describe_images(Filters=filters))["Images"]
        images = sorted(images,
                        key=lambda image: image['CreationDate'],
                        reverse=True)

    finally:
        if images: # returns True if len(images) >0
            logging.info(f"Found {len(images)} images for this search.")
            with open(fname, 'wb') as pfile:
                pickle.dump(images, pfile, protocol=pickle.HIGHEST_PROTOCOL)
            logging.debug(f"Saved results to disk at {fname}.")

    logging.debug(f"Found {len(images)} images for {prefix}.")
    return images


def publishToEs(images: list):
    """
    Publishes AMI descriptions to the configured elasticsearch instance.
    @param images: A list of images returned by the AWS API.
    """
    # ElasticSearch instance (url)
    es = elasticsearch.Elasticsearch([URL])

    # no do not delete the index
    # es.indices.delete(index=AMIINDEX, ignore=[400, 404])

    # ignore 400 cause by IndexAlreadyExistsException when creating an index
    es.indices.create(index=AMIINDEX, ignore=400)

    local_time = datetime.now(timezone.utc).astimezone()
    publish = True
    created, updated, ignored = 0, 0, 0

    for image in images:

        image["DocumentLastUpdated"] = local_time.isoformat()
        if image['CreationDate'] == '':
            image['CreationDate'] = '2000-06-01T00:00:00Z'

        if MOSTRECENT and MOSTRECENT > 0:
            publish = False
            # '2019-09-28T15:16:07.000Z'
            cDate = parser.parse(image['CreationDate'])
            td = local_time - cDate

            # Only publish if the AMI was created more recently than
            # the configured threshold
            if td.days <= MOSTRECENT:
                publish = True
            else:
                ignored += 1

        if publish:
            try:
                res = es.index(index=AMIINDEX, doc_type='ami-001',
                               body=image, id=image["ImageId"])
                if res['result'] == "updated":
                    updated += 1
                elif res['result'] == "created":
                    created += 1

            except RuntimeError:
                pass

    es.indices.refresh(index=AMIINDEX)
    logging.info(
        f"Finished updating elasticsearch index '{AMIINDEX}', "
        f"created: {created}, updated: {updated}, "
        f"ignored: {ignored} of {len(images)} images.")


def main():
    """
    Publish AWS AMIs to Elasticsearch.
    """
    logging.config.fileConfig('logconfig.ini')
    logging.debug("Starting run.")
    images = listAMIs()
    publishToEs(images)
    logging.debug("Ending run.")


if __name__ == "__main__":
    main()
